# Vyne Taxonomy Environment

This repository contains a docker-compose file which will set up Vyne suitable for
doing local taxonomy development

It runs the following components:

 * A vyne query server, which runs the UI
 * A local Eureka service, for registering services with if you want to test queries
 * A local schema server, for serving schemas from a local directory
 * A local cask, for storing data

# Prerequisties
 * docker 
 * docker-compose 
 * git (to clone this repository)

# Instructions
To get started, simply run

```bash
docker-compose up -d
``` 

Then wait a bit, and navigate to `http://localhost:9022`

## Using the default schema directory
By default, this environment will serve the schemas in the `./schemas` directory.

We've got a sample file there to get you started.
You can delete the contents of that directory, and replace with your own taxonomy

## Setting your own schema directory
If you've got a larger taxi project, you'll likely want to check it out
from source control.

Check it out somewhere, then either:

 * Update the `TAXONOMY_HOME` set in the .env file
 * OR set a `TAXONOMY_HOME` environment variable:
 
 ```bash
# linux / mac:
export TAXONOMY_HOME=/path/to/project

# Windows CMD
C:\> set TAXONOMY_HOME="c:\path\to\project"

# Windows PowerShell
PS C:\> $env:TAXONOMY_HOME="c:\path\to\project" 
```

The path you specify should either be:

 * A directory containing taxi files
 * A directory containing a `taxi.conf` file 
 
 